import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Title} from "@angular/platform-browser";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {MatTable} from "@angular/material/table";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-c_r',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.scss']
})
export class Test1Component implements OnInit, OnDestroy {
  private alMeds: any[];
  private form: FormGroup;
  private viewTable: Observable<any>;
  private selectedItem: any;
  constructor(private httpClient: HttpClient,
              private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              public dialog: MatDialog,
              title: Title) {
    title.setTitle('Meds')
  }

  ngOnInit(): void {
    this.httpClient
      .get<any>(environment.apiBaseUrl + `/get-all-meds`).subscribe(x => {
        this.alMeds = x.sort((a:any, b:any) => {
          return a.name.localeCompare(b.name);
        });
    })


    this.form = this.fb.group({
      periodicity: 'daily',
      weekDays: this.fb.array([]),
      recurrent: this.initRecurrent(),
      unit: 'pills',
      intakesArray: this.fb.array([])
    });


    this.viewTable.subscribe((res) => {
      if (res.action === 'details') {
        this.getMedecineByCode(res.element.id).then(x => this.selectedItem = x);
      } else if (res.action === 'edit') {
        this.router.navigate(['meds', res.element.id, 'edit' ],
          { queryParams: this.route.snapshot.queryParams }).then();
      } else if (res.action === 'preference') {
       // this.UpdatePreference(res.element);
      } else if (res.action === 'copy') {
        this.router.navigate(['meds', res.element.id,'copy'],
          { queryParams: this.route.snapshot.queryParams }).then();
      } else if (res.action === 'promote') {
       // this.requestPromotion(res.element);
      } else if (res.action === 'activate') {
        //  this.changestatus(res.element, true);
      } else if (res.action === 'deactivate') {
        //  this.changestatus(res.element, false);
      } else if (res.action === 'delete') {
        //  this.deleteMed(res.element);
      }
    });
  }

  initRecurrent() {
    return this.fb.group({
      everyAmount: new FormControl(null),
      everyType: null
    });
  }

  getMedecineByCode(code: any) {
    return this.httpClient
      .get(environment.apiBaseUrl + `/query?type=get_medicine&version=1&payload[code]=${code}`)
      .toPromise()
      .then(
        data => data, // success path
        error => {
          return;
        }
      );
  }

  delete(id: any) {
    return this.httpClient
      .delete(environment.apiBaseUrl + `/meds/${id}`)
      .toPromise()
      .then(
        data => data, // success path
        error => {
          return;
        }
      );
  }

  deletetreatment(e) {
    if (e.status === 'Stopped') {
      this.delete(e.id);
    }
    if (e.status === 'Active') {
      const message = `Not possible to delete ongoing treatment`;
      this.dialog.open(DialogComponent, {data: message});
    }
    if (e.status === 'Inactive') {
      const message = `Not possible to delete ongoing treatment.`;
      this.dialog.open(DialogComponent, {data: message});
    }
  }

  createTretment() {
    let obj = this.form.value;
    this.create(obj);

  }

  private create(data) {
    return this.httpClient
      .post(environment.apiBaseUrl + `/meds`), data
      .toPromise()
      .then(
        data => data, // success path
        error => {
          return;
        }
      );
  }



}
